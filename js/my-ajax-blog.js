'use strict'
$(document).ready(function(){
    $.ajax({
        url: 'data/my-ajax-blog.json',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            var len = response.length;
            for(var i=0; i<len; i++){
                var title = response[i].title;
                var image= response[i].image;
                var content = response[i].content;
                var firstCategoryElement = response[i].category[0];
                var secondCategoryElement = response[i].category[1];
                var thirdCategoryElement = response[i].category[2];
                var fourthCategoryElement = response[i].category[3];
                var fifthCategoryElement = response[i].category[4];
                // var sixthCategoryElement = response[i].category[5];
                var date = response[i].date;
                var blogEntry =
                    "<div class=\"card\">" +
                        "<div class=\"row\">" +
                            "<h1>" + title + "<h1>" +
                            "<div id='image'> " + "<img src=\" " + image + " \" alt=\"\" height=\"200 px\">" +
                            "</div>"+
                            "<p id='content'>" + content + "</p>" +
                        "</div>" +
                        "<div class=\"row\">" +
                            "<p>" + "Categories:" + "</p>" +
                        "</div>" +
                        "<div class=\"row\">" +
                            "<div class=\"col\">"+
                                "<p align='left'>" + firstCategoryElement + ", " + secondCategoryElement + ", " + thirdCategoryElement + ", " + fourthCategoryElement + ", " + fifthCategoryElement + "</p>" +
                                // "<p align='left'>" + secondCategoryElement + "</p>" +
                            "</div>" +
                            "<div class=\"col\">" +
                                "<p align='right'>" + date + "</p>" +
                            "</div>" +
                        "</div>" +
                    "</div>"
                $("#my-posts").append(blogEntry);
            }
        }
    });
});




// $.ajax('data/my-ajax-blog.json',{
//     type: 'Get',
// }).done(function (data) {
//     $.each(data, function (index, value) {
//     console.log(value)
    // var title = value.title;
    // var content = value.content;
    // var category = value.category;
    // var char = '<h1>' +
    //      + title + '</h1>' +
    //     '<p>' + content + '</p>' +
    //     '<p>' + category + '</p>'
    //     $('#my-posts').append(char);
// })
// })

// $.ajax('data/starWars.json',{
//     type: 'Get',
// }).done(function (data) {
//     $.each(data, function (index, value) {
//         console.log(value)
//         var name = value.name;
//         var height = value.height;
//         var mass = value.mass;
//         var birth_year = value.birth_year;
//         var gender = value.gender;
//         var char = '<tr>' +
//             '<td>' + name + '</td>' +
//             '<td>' + height + '</td>' +
//             '<td>' + mass + '</td>' +
//             '<td>' + birth_year + '</td>' +
//             '<td>' + gender + '</td>' + '</tr>'
//         $('#insertCharacters').append(char);
//     })
// })

/*

"title" : "Web Blog Post #1",
    "content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores in iusto odio praesentium quisquam, temporibus velit. Accusantium ad assumenda dignissimos doloremque eveniet nihil qui vero. Enim inventore maxime pariatur quos.",
    "category":[
    "family",
    "concerts",
    "food",
    "drink",
    "events"
]

*/


